\section{Zentrale Grenzwertsatz}
 Wir denken zunächst an eine unabhängige Folge von Zufallsgrößen \((X_n)_{n \in \N}, X_n \rightarrow \{0,1\}\), d.h. wir wiederholen beliebig oft ein \enquote{ja-nein-Experiment} (1-0, Treffer-Niete). \\
 Was passiert für \(n>>1\)? Wir wissen bereits:
 \[ \overline{X_n} \stoch \mu; \quad \overline{X_n} \fastsicher \mu \quad \quad \mu=\E(X_n)\]

Im Folgenden wollen wir dies präzisieren.
\subsection{Wurzel-n-Gesetz}
Betrachte das Balkendiagramm für z.B: \(p=\frac{1}{2}, n \text{ groß } (n= 10000)\).
Das \(\sqrt{n}\)-Gesetz besagt, dass man typischer Weise eine Trefferzahl von \(n \mu \pm \sqrt{n}\) erhält.
\begin{figure}[h]
 \centering
 \includegraphics[width=4cm]{Bilder/b6.jpg}
 %\caption{}
 \label{Bild6}
\end{figure}

\begin{notnumbered}{Erinnerung:}
\begin{itemize}
\item \( \P (|X_n - \mu| > \varepsilon) < \left(\frac{\sigma^2}{n \varepsilon^2}\right) \) \qquad (bzw. ebenso \(\dots < \frac{C \E(X^4)}{n^2 \varepsilon^4}\) und \(\dots < \frac{C \E(X^6)}{n^3 \varepsilon^6}\) )
\item \(\P (|nX_n - n \mu|>n \varepsilon) < \left(\frac{\sigma^2 n}{(n \varepsilon)^2}\right) \) \qquad (bzw. ebenso \(\dots < \frac{C \E(X^4) n^2}{(n \varepsilon)^4}\) und \(\dots < \frac{C \E(X^6)n^3}{(n \varepsilon)^6}\) )
\end{itemize}
\label{abschaetzungen}
\end{notnumbered}
und definiert \(n \varepsilon := b\)

Wählt man b proportional zu \(\sqrt{n}\) bzw. \(\varepsilon\) proportional zu \(n^{-0,5}\), dann ist die rechte Seite der Ungleichung konstant.\\
Wählt man ein b, so dass \(\frac{b}{\sqrt{n}} \rightarrow 0\), so ist die rechte Seite im Limes \(0\).

\begin{figure}[h]
 \centering
 \includegraphics[width=7cm]{Bilder/b7a.jpg}
 %\caption{}
 \label{Bild7a}
\end{figure}
\begin{figure}[h]
 \centering
 \includegraphics[width=7cm]{Bilder/b7b.jpg}
 %\caption{}
 \label{Bild7b}
\end{figure}

Wir sehen also, dass bei einem Zoomfaktor von \(b = \sqrt{n}\) bzw. \(\varepsilon = n^{-\frac{1}{2}}\) die Abschätzungen (alle drei in Erinnerung) fix bleiben.
In der Tat nähert sich das Balkendiagramm für \(n \rightarrow \infty\) bei entspr. \enquote{zoom} einer Gaußkurve an.

\begin{theorem}[Moivre-Laplace] Sei \((X_n)_{n \in \N}\) eine Folge unabhängiger gleich verteilter Zufallsgrößen mit \(X_n \rightarrow \{0,1\}\).\\
	Sei \(p\) die Wahrscheinlichkeit für \enquote{Treffer} \(\P(X_n=1)=p \quad \forall n \in \N\). \\
	Dann nähert sich die Folge
	\[Z_n = \frac{\sum_{j=1}^n X_n - n p}{\sqrt{n}}\] in Verteilung an \(N(0,p(1-p))\). \\
	(\(N(\mu,\sigma^2)\) ist die Normalverteilung (Dichte=Gauß) mit Erwartungswert \(= \mu\), Varianz \(= \sigma^2\))
\end{theorem}

\begin{proof}
	(eher Begründung): \\
	\(S_n = \sum_{j=1}^n X_n\) ist Binomialverteilt:
	\[\P (S_n = k) = B(p,n,k) = p^k(1-p)^{n-k} {\binom{n}{k}} \]
	Wir möchten zeigen, dass sich dies der entsprechenden Gaußkurve nähert für \(n \toinf\). \\
	\(n\) sei gerade, \(p = \frac{1}{2}\), setze \(x=k- \frac{n}{2} (n-p)\)
	\begin{figure}[h]
	 \centering
	 \includegraphics[width=7cm]{Bilder/b8.jpg}
	 %\caption{}
	 \label{Bild8}
	\end{figure}
	\[\P(S_n=k) = \left(\frac{1}{2}\right)^n \cdot \frac{n!}{k!\cdot (n-k)!}\]
	Um zu sehen, dass \(\frac{n!}{k! (n-k)!}\) sich an eine Gaußkurve annähert betrachte
	\[f(x) = \frac{n!}{(x+\frac{n}{2})!(n-(x+\frac{n}{2}))!} = \frac{n!}{(x+\frac{n}{2})!(\frac{n}{2}-x)!} \quad \quad (f(x)=(f(-x))\]
	Betrachte \(f(x+1)-f(x)\). Dies ist eine Art Ableitung.
	
	\begin{figure}[h]
	 \centering
	 \includegraphics[width=7cm]{Bilder/b9.jpg}
	 %\caption{}
	 \label{Bild9}
	\end{figure}
	Da die Schrittlänge \(1\) sehr klein ist im Vergleich zur Breite der Funktion \((\sim\sqrt{n})\), ist \(f'(x) \approx f(x+1)-f(x)\).
	\[f(x+1)-f(x) = \frac{n!}{(\frac{n}{2}+x+1)!(\frac{n}{2}-x-1)!} - \frac{n!}{(\frac{n}{2}+x)!(\frac{n}{2}-x)!} \]
	\[ = \frac{n!(\frac{n}{2}-x)}{(\frac{n}{2}+x+1)(\frac{n}{2}+x)!(\frac{n}{2}-x-1)!(\frac{n}{2}-x)} - \frac{n!}{(\frac{n}{2}+x)!(\frac{n}{2}-x)!}\]
	\[ = \frac{n!}{(\frac{n}{2}+x)!(\frac{n}{2}-x)!} \cdot (\frac{\frac{n}{2}-x}{\frac{n}{2}+x+1}-1) \]
	\[ = f(x) \frac{\frac{n}{2}-x-(\frac{n}{2}+x+1)}{\frac{n}{2}+x+1} = f(x) \frac{-2x-1}{ \frac{n}{2}+ x+1}\]
	Wir machen eine zweite Näherung: Da \(x \sim \sqrt{n},\ -2x-1 \approx -2x,\ \frac{n}{2}+x+1 \approx \frac{n}{2}\)
	\[f'(x) \approx f(x) \frac{-2x}{\frac{n}{2}} = f(x)\left(\frac{-4}{n}\right)x\]
	\[\frac{df}{dx} = f \cdot \left(\frac{-4}{n}\right)x\]
	\[\int \frac{df}{f} = \int \frac{-4}{n}x dx\]
	\[\ln f = \frac{-4}{n}\frac{x^2}{2}+C \quad | e^{\dots}\]
	\[f = e^{-\frac{x^2}{2(\frac{n}{4})}}\cdot C' \quad \quad (C' = e^C)\]
	
	\begin{figure}[h]
	 \centering
	 \includegraphics[width=3cm]{Bilder/b10.jpg}
	 %\caption{}
	 \label{Bild10}
	\end{figure}
	Wähle C' in Hinblick auf Normierungsbedingung. \\
	\(\Rightarrow Z_n = \frac{S_n - p n}{\sqrt{n}}\) nähert sich also in Verteilung an \(N(0,\frac{1}{4}) = N(0,p(1-p))\)
\end{proof}

\underline{Bemerkung}: Die Konvergenzgeschwindigkeit ist hier nicht erbracht. \\
\underline{Anwendung/Beispiel:} Da die Verteilungskonvergenz gilt, gilt auch \(\P(a \leq S_n \leq b) \approx V_x(b)-V_x(a)\) X normalverteilt mit entspr. \enquote{zoom}. \\

\begin{bsp}
Sei \(p=\frac{1}{2} \quad n=1000\)
\[\P(4900 < S_n \leq 5100) = \P(-100 < S_n - 5000 \leq 100) =\]
\[=\P(-1 < \frac{S_n-5000}{100} \leq 1) = \int_{-1}^1 \frac{1}{\sqrt{2 \pi}\frac{1}{2}}\cdot e^{-\frac{x^2}{2\cdot \frac{1}{4}}}dx = \dots \text{im Tafelwerk nachschauen}\]
\end{bsp}

\begin{theorem}[Zentraler Grenzwertsatz]
	Sei \((X_n)_{n\in\N}\) Folge von unabhängigen, identisch verteilten Zufallsvariablen mit Erwartungswert \(\mu < \infty\) und Varianz \(\sigma < \infty\). Dann konvergiert die Folge 
	\[ z_n \defeq \dfrac{\sum\limits_{j=1}^n (X_j - \mu)}{\sqrt{n}}\]
	in Verteilung gegen \(N(\Theta, \sigma)\) (Normalverteilung).
\end{theorem}
\underline{Bemerkung}: Die Normalverteilung hat die Eigenschaft, dass bei Faltungen von Gaußkurven wieder Gaußkurven entstehen. D.h. \[X, Y \text{ normalverteilt } \Rightarrow X + Y \text{ normalverteilt}\]
und die Varianzen addieren sich (vorausgesetzt \(X,Y\) unabhängig). Diese Eigenschaft kann man benutzen um Moivre-Laplace zu verallgemeinern.

\subsection{Brown'sche Bewegung}
Wir betrachten eine Irrfahrt in einer Dimension, Zeit und Ort diskret. Gegeben \(N >> 1\) Teilchen mit positiven \(x_j^0\) zur Zeit 0. In jedem Zeitschritt \(dt\) kann jedes Teilchen einen Schritt \(dx\) nach links oder rechst (\(+dx, -dx\)) unternehmen. Wir nehmen an, dies geschieht mit \(p=\frac12\) zufällig und unabhängig voneinander (unabhängig in Zeit und Teilchenindex).

Fragen: 
\begin{itemize}
	\item Wo sind die Teilchen zu späterer Zeit?
	\item Wie sieht die Teilchendichte als Funktion von Ort und Zeit aus?
\end{itemize}

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=7cm]{Bilder/brownsche-bewegung-ameisen.jpg}
		\caption{\enquote{Ameisen am Ort \( dx\) z.B.}}
		\label{BildBrownscheBewegungAmeisen}
	\end{center}
\end{figure}

\(\rho_t(x)\) beschreibt die erwartete Anzahl an Teilchen am Ort \(x \in dx \cdot \Z\) zur Zeit \(t\) (\(t \in dt \cdot \N_0\))

Sei \(\rho_t(x)\) gegeben. Wie sieht \(\rho_{t+dt}(x)\) aus? \(\rho_{t+dt}(x)\) hängt ab von \(\rho_t(x-dx)\) und \(\rho_t(x+dx)\) (Teilchen machen nur 1 Schritt).

\begin{figure}[h]
\begin{minipage}{0.5\textwidth}
	\begin{center}
		\includegraphics[width=5cm]{Bilder/brownsche-bewegung1.jpg}
		\label{BildBrownscheBewegung1}
	\end{center}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\underline{1. Fall}: \(\rho_t(x)\) ist konstant.\\
\(\rho_{t+dt} (x) \) ist ebenfalls konstant. \(\Rightarrow \rho_{t+dt} - \rho_t = 0\)
\end{minipage}
\end{figure}


\underline{2. Fall}: \(\rho_t(x)\) ist linear: \(\rho_t(x) = m (x-x_0) + c\). \\
Betrachte \(\rho_{t+dt}(x_0) - \rho_t(x_0) = 0\)

\begin{figure}[h]
\begin{minipage}{0.5\textwidth}
	\begin{center}
		\includegraphics[width=5cm]{Bilder/brownsche-bewegung2.jpg}
		\label{BildBrownscheBewegung2}
	\end{center}
\end{minipage}
\begin{minipage}{0.5\textwidth}
Alle Teilchen von \(x_0\) laufen weg (müssen nach rechts oder links). Von links kommen wegen \(p=\frac12\) ungefähr \(\frac12 n (x_0 - dx)\) zu \(x_0\) und von rechts kommen \(\frac12 n (x_0+dx)\).
\end{minipage}
\end{figure}

\[\frac12 (m\cdot dx + c) + \frac12 (m(-dx) + c) -c = 0\]

\underline{3. Fall}: Berechne \(\rho_{t+dt}(x_) - \rho_t(x_0)\) für \(\rho_t(x) = a(x-x_0)^2\)

\begin{figure}[h]
\begin{minipage}{0.5\textwidth}
	\begin{center}
		\includegraphics[width=5cm]{Bilder/brownsche-bewegung3.jpg}
		\label{BildBrownscheBewegung3}
	\end{center}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\[\rho_{t+dt}(x_0) = \frac12 \rho_t(x_0+dx) + \frac12\rho_t(x_0)-dx \]
\[= \frac12 \left(a\;dx^2 + a \;dx^2\right) = a\; dx^2 \overset{\rho'' = 2a}{=} \frac{\rho''}{2}(x_0)\; dx^2\]
\end{minipage}
\end{figure}

Höhere Ableitungen spielen keine Rolle, da sie Beiträge der Ordnung \(dx^3, dx^4, \dots\) liefern. Für \(dx\) klein sind diese Beiträge zu vernachlässigen.

\begin{align*}
	\Rightarrow \rho_{t+dt}(x) - \rho_t(x) &= \frac{\rho''}{2} \; dx^2  \qquad |: dt \\
	\frac{\rho_{t+dt}(x) - \rho_t(x)}{dt} &= \frac{\rho''}{2} \; \frac{dx^2}{dt} \qquad \text{Wähle }dx^2 = dt \\
	\Rightarrow \dot{\rho}_t(x) &= \frac12 \rho''_t(x)  \quad (\text{Wärmeleitungsgleichung})
\end{align*}

Das ganze hat eine Menge mit dem Zentralen Grenzwertsatz zu tun. Ein einzelnes Teilchen bewegt sich nie im Galtonbrett
\[X_{n\, dt}^1 = \sum_{j=1}^n Y_j\, dx \qquad Y_j \in \{ \pm 1 \} \text{ je mit } p=\frac12 \]

Für \(t \approx 1\) ist \(n = \frac1{dt}\). Die Breite von \(\sum_{j=1}^n Y_j \) ist \(\sqrt{\frac1{dt}}\). Soll \(X_{n\, dt}^1 \) die Breite \(1\) haben \(\ dx \cdot \sqrt{\frac1{dt}}\) (wie vorher \(dx^2 = dt\) ). Jedes Teilchen ist also in guter Näherung verteilt wie \(X_t^j \inVert N(X_o^j, \sqrt{t})\) (für \(t = n \cdot dt\)), wobei \(N\) die Normalverteilung ist.
\[ \Rightarrow \rho_t(x) = \int \rho_0(y) \frac{1}{\sqrt{2\pi t}} e^{\frac{-(x-y)^2}{2t}} \, dy \]
\[ \dot{\rho}_t(x) = \int \rho_0(y) \left( \frac{1}{2\pi t^{\frac{3}{2}}} e^{\frac{-(x-y)^2}{2t}} + \frac{1}{\sqrt{2\pi t}} e^{\frac{-(x-y)^2}{2t}} \left( \frac{(x-y)^2}{2t^2}\right) \right)\, dy\]
Überprüfe nun \(\rho''_t(x) \Rightarrow \checkmark\)

%3.7.2017

\subsection{Anwendung: Ruinwahrscheinlichkeit}
Wir spielen in einer Art Casino mit Startkapital S immer das gleiche Spiel, d.h. in jeder Spielrunde gewinnen wir mit Wahrscheinlichkeit \(p\) einen Euro und verlieren mit Wahrscheinlichkeit \(q\) einen Euro \((p+q=1)\). Wir verlassen das Casino bei Ruin oder einem vorher bestimmten Betrag \(G>S\). \\
Wie hoch ist die Wahrscheinlichkeit, dass ich mit Gewinn das Casino verlasse? \\

\begin{figure}[h]
 \centering
 \includegraphics[width=7cm]{Bilder/b11.jpg}
 %\caption{}
 \label{Bild11}
\end{figure}

\underline{Lösung:} \\
Annahme \(p=q=\frac{1}{2}\) (allg. Fall: Übung.) \\
Klar: Falls \(G=2S\), dann ist die Ruinwahrscheinlichkeit=Gewinnwahrscheinlichkeit = \(\frac{1}{2}\). (Symmetrie). \\
Sei \(p_k\) die Wahrscheinlichkeit für den Ruin, falls mein Kapital \(k\) beträgt.
\[p_k = \frac{1}{2}p_{k+1}+\frac{1}{2}p_{k-1}\] Setze \(p_{k+1}-p_k =: d_k\).
\[\Rightarrow \frac{1}{2}p_{k+1}-\frac{1}{2}p_k = \frac{1}{2}p_k- \frac{1}{2}p_{k-1}\]
\[\Rightarrow d_k = d_{k-1}\]

\begin{figure}[h]
 \centering
 \includegraphics[width=7cm]{Bilder/b12.jpg}
 %\caption{}
 \label{Bild12}
\end{figure}

\subsection{Zurück zum \(\sqrt{n}\)-Gesetz}
\label{sub:zurück_zum_wurzel_n_Gesetz}
\underline{Situation:} \((X_n)_{n \in \N}\) Folge von Zufallsgrößen, unabhängig, gleichverteilt. \(X_n : \Omega \rightarrow \{0,1\}, \  p=\P(X_n=1), \\P(X_n=0)=q\)\\
\underline{Frage:} Welche Anzahl erhält man typischer Weise bei \(n\) Versuchen? \\
\underline{Antwort:} \(p \cdot n \pm \sqrt{n}\) \\
Das soll quantifiziert werden:\\
Wie hoch ist die Wahrscheinlichkeit \(\P(S_n \in [p \cdot n - \sqrt{n},p \cdot n + \sqrt{n} ])\) Hinweis: \(S_n = \sum_{j=1}^n X_j\). \\
Wegen dem zentralen Grenzwertsatz gilt:
\[\P(p n -\sqrt{n} \leq S_n \leq p n + \sqrt{n}) = \P(-\sqrt{n} \leq S_n - p n \leq \sqrt{n})\]
\[= \P\left(-1 \leq \frac{S_n - p n}{\sqrt{n}} \leq 1\right) = \int_{-1}^1 \frac{1}{\sqrt{2 \pi}\sigma}e^{-\frac{x^2}{2 \sigma^2}}dx\]
Substitution: \(\frac{x}{\sigma}=y \quad (\frac{dy}{dx}=\frac{1}{\sigma})\).
\[\P(S_n \in [p n - \sqrt{n}, p n + \sqrt{n}]) \approx \int_{-\frac{1}{\sigma}}^{\frac{1}{\sigma}} \frac{1}{\sqrt{2 \pi}}e^{-\frac{y^2}{2}}dy\]
Formel: \(p (1-p) \leq \frac{1}{4}\) (falls \(p\in [0,1]\)) \\
\((x+\frac{1}{2})(\frac{1}{2}-x)\)

\begin{figure}[h]
 \centering
 \includegraphics[width=7cm]{Bilder/b13.jpg}
 %\caption{}
 \label{Bild13}
\end{figure}

\[\E(X_i) = p\]
\[\E (X_i^2) = \E(X_i) = p\]
\[\Var(X_i) = \E(X_i^2)-\E(X_i)^2=p-p^2=p(1-p)=p q\]
\[\sigma \leq \frac{1}{2}\]
\[\Rightarrow \P(S_n \in [p n - \sqrt{n}, p n + \sqrt{n}]) \geq \int_{-2}^{2} \frac{1}{2 \pi}e^{-\frac{y^2}{2}}dy \geq 95\%\] (Nachschlag im Tafelwerk)
