\select@language {german}
\contentsline {section}{\numberline {1}Endliche Wahrscheinlichkeitsr\IeC {\"a}ume}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Axiome von Kolmogorov}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Bedingte Wahrscheinlichkeit}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Stochastische Unabh\IeC {\"a}ngigkeit}{5}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Paradoxa}{6}{subsection.1.4}
\contentsline {subsubsection}{\numberline {1.4.1}Simpson Paradoxon}{6}{subsubsection.1.4.1}
\contentsline {subsubsection}{\numberline {1.4.2}Ziegenproblem}{6}{subsubsection.1.4.2}
\contentsline {subsection}{\numberline {1.5}Regeln am Baumdiagramm}{7}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Zufallsgr\IeC {\"o}\IeC {\ss }en}{7}{subsection.1.6}
\contentsline {subsubsection}{\numberline {1.6.1}Erwartungswert (gewichtete Wahrscheinlichkeiten)}{8}{subsubsection.1.6.1}
\contentsline {subsubsection}{\numberline {1.6.2}Varianz und Standardabweichung (Ma\IeC {\ss } f\IeC {\"u}r die Streuung / das Risiko)}{9}{subsubsection.1.6.2}
\contentsline {subsection}{\numberline {1.7}Ungleichung von Markov und Tschebyschew}{10}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Mehrere Zufallsgr\IeC {\"o}\IeC {\ss }en}{11}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}Das schwache Gesetz der gro\IeC {\ss }en Zahlen}{13}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}Einschub: Nicht-Lokalit\IeC {\"a}t, Korrelationen in der Natur}{14}{subsection.1.10}
\contentsline {section}{\numberline {2}\IeC {\"U}berabz\IeC {\"a}hlbares \(\Omega \)}{16}{section.2}
\contentsline {section}{\numberline {3}Zufallsgr\IeC {\"o}\IeC {\ss }en}{19}{section.3}
\contentsline {section}{\numberline {4}Verteilungsfunktion}{20}{section.4}
\contentsline {section}{\numberline {5}Mehrere Zufallsgr\IeC {\"o}\IeC {\ss }en}{23}{section.5}
\contentsline {section}{\numberline {6}Erwartungswert}{23}{section.6}
\contentsline {section}{\numberline {7}Folgen von Zufallsgr\IeC {\"o}\IeC {\ss }en}{25}{section.7}
\contentsline {subsection}{\numberline {7.1}Stochastische Konvergenz}{25}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Fast sichere Konvergenz}{26}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Satz von Borel-Cantelli}{27}{subsection.7.3}
\contentsline {section}{\numberline {8}Das starke Gesetz der gro\IeC {\ss }en Zahlen}{28}{section.8}
\contentsline {section}{\numberline {9}Konvergenz in Verteilung}{29}{section.9}
\contentsline {section}{\numberline {10}Zentrale Grenzwertsatz}{32}{section.10}
\contentsline {subsection}{\numberline {10.1}Wurzel-n-Gesetz}{33}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Brown'sche Bewegung}{37}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Anwendung: Ruinwahrscheinlichkeit}{38}{subsection.10.3}
\contentsline {subsection}{\numberline {10.4}Zur\IeC {\"u}ck zum \(\sqrt {n}\)-Gesetz}{39}{subsection.10.4}
\contentsline {section}{\numberline {11}Hypothesentests}{40}{section.11}
\contentsline {subsection}{\numberline {11.1}Maximum Likelyhood}{41}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Hypothesentest - Framework}{42}{subsection.11.2}
\contentsline {subsection}{\numberline {11.3}Alternativhypothese \(H_1\)}{43}{subsection.11.3}
\contentsline {subsection}{\numberline {11.4}p-Wert (eines Tests)}{43}{subsection.11.4}
\contentsline {subsection}{\numberline {11.5}Einseitiger Test}{45}{subsection.11.5}
\contentsline {subsection}{\numberline {11.6}Das Neyman-Pearson Lemma}{47}{subsection.11.6}
\contentsline {section}{\numberline {12}Der \(\chi ^2\)-Test}{48}{section.12}
\contentsline {subsection}{\numberline {12.1}Verallgemeinerung f\IeC {\"u}r \(f=2 (k=3)\):}{49}{subsection.12.1}
\contentsline {subsection}{\numberline {12.2}Allgemeiner Fall:}{50}{subsection.12.2}
\contentsline {section}{\numberline {13}Erwartungswert f\IeC {\"u}r allgemeine Situation}{51}{section.13}
\contentsline {subsection}{\numberline {13.1}Lebesgue Integral}{53}{subsection.13.1}
