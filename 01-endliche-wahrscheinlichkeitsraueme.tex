Im Laufe der Vorlesung werden wir der konzeptionellen Bedeutung des Begriffs Wahrscheinlichkeit näher kommen. Unter der Aussage \enquote{Wahrscheinlichkeit für eine \( 6 \) ist \( \frac16 \)} stellen wir uns zunächst folgende Bedeutung vor: Bei häufigem Wiederholen der Würfelexperimente unter gleichen Bedingungen erhält man typischer Weise eine relative Häufigkeit an 6ern von \( \frac16 \).

\section{Endliche Wahrscheinlichkeitsräume}
Es geht in der Stochastik um die Beschreibung von Experimenten. Alle denkbaren Ausgänge eines Experiments fassen wir in einer Menge, der sogenannten \textbf{Ergebnismenge \(\Omega\)}, zusammen. Die Potenzmenge von \(\Omega\) nennen wir \textbf{Ereignismenge}
\subsection{Axiome von Kolmogorov}
\begin{definition}[Axiome von Kolmogorov (für Wahrscheinlichkeitsmaß)]
Eine Abbildung \( \P:\; \mathcal{P}(\Omega) \to \R \) heißt \textbf{Wahrscheinlichkeitsmaß}, wenn gilt:
\begin{enumerate}
	\item \label{axiomA1}\(\P(A) \geq 0 \qquad \forall A \in \mathcal{P}(\Omega)\)
	\item \label{axiomA2} \(\P(\Omega) = 1\)
	\item \label{axiomA3} \(\P( A \cup B ) = \P(A) + \P(B) \qquad \forall A \cap B = \emptyset \)
\end{enumerate}
\end{definition}

Dies sind Axiome im klassischen Sinne, d.h. durch das Konzept gegebene Wahrheiten.

\begin{theorem}
Sei \(\Omega\) endlich, \(\P:\; \mathcal{P}(\Omega) \to \R \) ein Wahrscheinlichkeitsmaß, dann gilt:
\begin{enumerate}[a)]
	\item \( \P(A) \leq 1 \qquad \forall A \in \mathcal{P}(\Omega)\)
	\item \(\P(A \cup B) = \P(A) + \P(B) - \P(A \cap B)\)
\end{enumerate}
\end{theorem}

\begin{proof} ... \end{proof}

\underline{Bemerkung}: Im endlichen Fall ist \(\P\) durch die Wahrscheinlichkeit der Elementarereignisse, also durch \(\P(\{\omega\})\) \(\forall \omega \in \Omega\) eindeutig gegeben. Die Wahrscheinlichkeit von \( A \) können wir durch Axiom \ref{axiomA3} 
und zerlegen von \(A\) in die Elementarereignisse bestimmen.

\underline{Bemerkung}: \textbf{Das} Wahrscheinlichkeitsmaß für ein bestimmtes Experiment zu finden ist i.d.R. schwierig

\underline{Laplace-Annahme}: Falls man aus der Physik einen Grund zur Annahme hat, dass die denkbaren Ausgänge (die Elemente von \(\Omega\)) alle gleich plausibel sind, erhält man \(\P(A) = \dfrac{|A|}{|\Omega|}\)

\subsection{Bedingte Wahrscheinlichkeit}

\underline{Motivation:} Wir interessieren uns nur für diejenigen Ausgänge eines Experimentes, die einer gewissen Bedingung genügen, zum Beispiel, dass das Experiment in einem gewissen Sinne erfolgreich war. Ist \(\P\) für das volle Experiment gegeben, erhält man nun ganz natürlich ein Wahrscheinlichkeitsmaß für ein \enquote{neues} Experiment, die sogenannte \textbf{bedingte Wahrscheinlichkeit}.

\begin{definition}
	Sei \(\Omega\) endlich, \(A \subseteq \Omega\) (\enquote{Experiment ging gut}). Dann ist \(\P_A: \mathcal{P}(\Omega) \to \R \) gegeben durch
	\[\P_A(B) = \dfrac{\P(A \cap B )}{\P(A)}\]

	Bedingung ist natürlich \(\P(A) \neq 0\). \(\P_A\) nennt man \textbf{bedingte Wahrscheinlichkeit}.
\end{definition}
\begin{theorem}
	Für alle \(A\) mit \(\P(A) \neq 0\) ist \(\P_A\) ein Wahrscheinlichkeitsmaß.
\end{theorem}

\underline{Bemerkung}: Man kann sich vergewissern, dass \(\P_A\) in der Tat das liefert, was in der Motivation gefordert wurde: Sei zum Beispiel \(B \subseteq A \). Wir wiederholen 1000 mal das Experiment. In \(n_A\) Fällen ist das Experiment geglückt. In \(n_B\) erhielten wir B.

\[ \P_A(B) = \frac{\P(B)}{\P(A)} \approx \dfrac{\frac{n_B}{1000}}{\frac{n_A}{1000}} = \frac{n_B}{n_A}\]

\begin{proof}....\end{proof}

\begin{theorem}[Satz von Bayes]Seien \( A,B \subseteq \Omega  \) zwei Ereignisse mit \(\P(A), \P(B) \neq 0, \P(A \cap B) \neq 0 \), dann gilt: 

\[ \frac{ \P_B(A)}{\P_A(B)} = \frac{\P(A)}{\P(B)}\]

\end{theorem}
\begin{proof}
\(\dfrac{\P_B(A)}{\P_A(B)} = \dfrac{\P(A\cap B)}{\P(B)} : \dfrac{\P(A\cap B)}{\P(A)} = \dfrac{\P(A)}{\P(B)}
\)
\end{proof}

\underline{Bemerkung}: Falls \(\P(A\cap B) = 0 \) gilt \( \P_B(A) = 0 = \P_A(B) \)

\subsection{Stochastische Unabhängigkeit}
\begin{definition}
	Seien \(A, B \subseteq \Omega\). \( A \) ist stochastisch \textbf{unabhängig} von \( B \), genau dann wenn gilt:
	\[\P(A \cap B) = \P(A) \cdot \P(B) \]
\end{definition}
\begin{theorem}
	Seien \( A, B \subseteq \Omega \) unabhängig (bzgl. Wahrscheinlichkeitsmaß \(\P\)) \(\P(A) \neq 0\), dann gilt:  \[ \P_A(B) = \P(B) \]
\end{theorem}

\underline{Bemerkung}: Es gilt auch die Umkehrung, d.h. falls \( \P(A) \neq 0 \) gilt die Implikation \( \P_A(B) = \P(B) \Rightarrow A,B \) unabhängig. Außerdem: \(\P_A(B) = \P(B) \Rightarrow A,B \) unabgängig \( \Rightarrow \P_B(A) = \P(A) \) falls \( \P(A), \P(B) \neq 0 \)

\underline{Bemerkung}: Bei drei oder mehr Mengen gibt es die Begriffe \textbf{\enquote{paarweise Unabhängigkeit}} (schwach) und \textbf{\enquote{Unabhängigkeit}} (stark)

\begin{definition}
	Die Mengen \((A_1, A_2, \dots, A_n), n \in \N \) heißen \textbf{paarweise unabgängig}, falls \(A_i\) unabgängig von \(A_j\) \(\forall i \neq j \) mit \(i, j \in \{1, \dots, n\}\)
\end{definition}

\begin{definition}
	\((A_1, \dots, A_n)\) (wie oben) sind \textbf{unabhängig} \( : \Leftrightarrow \) Für jede Teilmenge \(\sigma \subseteq \{1,\dots, n\},\  |\sigma| \geq 2 \) gilt:

	\[\P\left( \bigcap_{i\in \sigma} A_i \right) = \prod_{i\in \sigma} \P(A_i)\]
\end{definition}

\underline{Bemerkung}: Offensichtlich gilt Unabhängigkeit \(\Rightarrow\) paarweise unabgängig, denn dies ist genau der Fall \(|\sigma| = 2 \). Die Umkehrung (d.h. paarweise unabhängig \(\Rightarrow\) unabgängig) \textbf{gilt nicht}! Das beweisen wir mit Hilfe eines Gegenbeispiels.

\begin{bsp}
	Wir werfen 2 mal unabhängig eine Münze. Sei Kopf = 1, Zahl = -1\\
	\(\Omega = \{(1,1), (1,-1), (-1,1), (-1,-1)\}\)\\
	\(A = \{(1,1), (1,-1)\}, \quad B = \{ (1,1), (-1,1) \}, \quad C = \{ (1,1), (-1,-1) \}\)

	\(\P(A\cap B) = \P(\{(1,1)\}) = \frac14 = \frac12 \cdot \frac12 = \P(A)\cdot\P(B) \qquad
	\P(A\cap C) = \P(\{(1,1)\}) = \frac14 = \P(A) \cdot \P(C) \)\\
	aber: \(\P(A\cap B \cap C) = \P(\{(1,1)\}) = \frac14 \neq \frac18 = \P(A) \cdot \P(B) \cdot \P(C)\)
\end{bsp}

\subsection{Paradoxa}
\subsubsection{Simpson Paradoxon}

Wir betrachten Daten zu Todesurteilen in Florida vor 1973

\begin{center}
	\begin{tabular}{c|c|c|c}
	 & ja & nein & \\ \hline
	 schwarz & 59 & 2448 & 2,4\% \\
	 weiß & 72 & 2158 & 3,4\% \\
	\end{tabular}
\end{center}
Es scheint, dass die Gerichte bei schwarzen Tätern weniger streng waren. Betrachtet man die Daten genauer, ergibt sich ein anderes Bild.

\begin{center}
	\begin{tabular}{cc}
	Opfer weiß & 
	Opfer schwarz \\
	\begin{tabular}{c|c|c|c}
	 & j & n & \\ \hline
	 s & 48 & 239 & 16,7 \% \\
	 w & 72 & 2047 & 3,4 \% \\
	\end{tabular}
&
		\begin{tabular}{c|c|c|c}
		 & j & n & \\ \hline
		 s & 11 & 2209 & 0,5 \% \\
		 w & 0 & 111 & 0 \% \\
		\end{tabular}
	\end{tabular}
\end{center}

Bei Korrelationen sollte man generell vorsichtig sein! Dieser Effekt, dass das Ergebnis bzw. die Aussage sich bei näherem Anschauen umdreht, heißt auch Simpson-Paradoxon.

\subsubsection{Ziegenproblem}
Beim Ziegenproblem geht es um folgendes Spiel: Man kann eines von drei Toren wählen, hinter einem befindet sich ein Gewinn, hinter zweien eine Ziege (\(\P(\text{Gewinn}) = \frac13\)). Nachdem man sich für ein Tor entschieden hat, öffnet der Moderator eines der beiden anderen Tore, hinter dem sich eine Ziege befindet. Man darf nun das Tor wechseln. Sollte man das tun?

Antwort: Das ist anhand der Informationen nicht ganz klar. Öffnet der Moderator evtl. das Tor, um uns zu verwirren (weil wir gerade den Gewinn gezogen haben)? Falls von vorne herein klar war, dass der Moderator in jedem Fall ein Tor mit einer Ziege öffnet, dann ist ein Wechsel sinnvoll (siehe Baumdiagramm in Abbildung \ref{BildBaumdiagramm})!

\begin{figure}[h]
 \centering
 \includegraphics[width=10cm]{Bilder/baumdiagramm.jpg}
 \caption{\(\P_\text{tauschen}(\text{Gewinn}) = \frac23 \quad \P_\text{nicht tauschen}(\text{Gewinn}) = \frac13 \)}
 \label{BildBaumdiagramm}
\end{figure}

\subsection{Regeln am Baumdiagramm} An den Ästen stehen bedingte Wahrscheinlichkeiten!
\begin{enumerate}[a)]
	\item \label{Pfadregel1} Entlang der Äste wird multipliziert
	\item \label{Pfadregel2} Die Knotenwahrscheinlichkeiten werden addiert
	\item \label{Pfadregel3} Die wegführenden bedingten Wahrscheinlichkeiten an jedem Knoten addieren sich zu 1
\end{enumerate}

\underline{Bemerkung}: die jeweiligen Ereignisse, in die man aufteilt, müssen disjunkt sein, damit \ref{Pfadregel2} und \ref{Pfadregel3} gelten.


\subsection{Zufallsgrößen}
\underline{Motivation}: Um weitere Mathematik betreiben zu können, bilden wir \(\Omega \to \R \) ab. Das geschieht meist ganz natürlich, zum Beispiel bei Wetten auf ein Experiment. (Verallgemeinerung auf andere Mengen statt \(\R\) ist ebenfalls möglich.)

\begin{bsp}
	Würfel mit verschiedenen Farben bemalt. 

	\( \text{rot} \mapsto 1, \text{blau} \mapsto 2, \dots\)
\end{bsp}

\begin{definition}
	Sei \((\Omega, \P)\) ein Wahrscheinlichkeitsraum, d.h. \(\Omega\) eine Menge, \(\P: \mathcal{P} (\Omega) \to \R \) ein Wahrscheinlichkeitsmaß. Eine Abbildung \(X : \Omega \to \R\) nennt man (reelle) \textbf{Zufallsgröße} (=\textbf{Zufallsvariable}).
\end{definition}
 Mit Hilfe der Zufallsgrößen können wir z.B. ausrechnen, ob eine Wette fair ist oder nicht

\subsubsection{Erwartungswert (gewichtete Wahrscheinlichkeiten)}

\begin{definition}
	Sei \(X : \Omega \to \R \) eine Zufallsgröße, \(\P\) sei gegeben. Dann nennt man \[\E(X) \defeq \sum_{\omega \in \Omega} \P(\{\omega\}) \cdot X(\omega) \] den \textbf{Erwartungswert} von \(X\). Falls \(\E(X) = 0\) spricht man von einer \textbf{fairen Zufallsgröße} bzw. einem \textbf{fairen Spiel}.
\end{definition}

\begin{bsp}
	Man setzt 1€ auf rot beim Roulette, \(X\) beschreibt dabei den Gewinn in Euro. \[X: \{0,\dots,36\} \to \{-1,1\} \subseteq \R\] 19 Zahlen (grün, schwarz) werden auf -1 abgebildet, 18 Zahlen (rot) auf 1.

	\[\E(x) = \sum_{\omega \in \{0,\dots,36\}} \frac{1}{37} \cdot X(\{\omega\}) = - \frac1{37}\]
\end{bsp}

\begin{theorem}[Formel] Sei \(X:\Omega \to A \subseteq \R \) eine Zufallsgröße, dann gilt:
\[ \E(X) = \sum_{a \in A} a \cdot \P(X = a) \]
wobei mit \(X = a\) das Ereignis \(\{\omega : X(\omega) = a \}\) gemeint ist.	
\end{theorem}
\begin{proof}
	...
\end{proof}

\begin{theorem}
	Sei \((\Omega, \P)\) ein Wahrscheinlichkeitsraum, \(X, Y\) Zufallsgrößen auf \(\Omega\), \(\alpha \in \R\). Dann gilt:
	\[ \E ( \alpha X + Y ) = \alpha \cdot \E(X) + \E(Y)\]
	Der Erwartungswert ist also linear. 
\end{theorem}

\begin{proof}
...
\end{proof}

\subsubsection{Varianz und Standardabweichung (Maß für die Streuung / das Risiko)}

\underline{Motivation}: Neben dem Erwartungswert wünschen wir uns eine Größe, die das Risiko quantifiziert. Ein geeigneter Kandidat ist die Varianz auf \(X\).

\begin{definition}
	Gegeben \((\Omega, \P)\), \(X\) Zufallsgröße. Dann nennt man

	\[\Var(X) \defeq \E\left( \big(X-\E(X)\big)^2\right)\]


	die \textbf{Varianz} von X.
\end{definition}

\begin{figure}[h]
\begin{center}
 \includegraphics[width=5cm]{Bilder/balkendiagramm1.jpg}
 %\caption{}
 \label{BildBalkendiagramm1}
 \hspace{1cm}
 \includegraphics[width=5cm]{Bilder/balkendiagramm2.jpg}
 %\caption{}
 \label{BildBalkendiagramm2}
 \caption{Beispiel Roulette - Das linke Bild zeigt die Wahrscheinlichkeiten für die Ausgänge beim Setzen auf eine Farbe, das rechte die beim Setzen auf eine einzelne Zahl. Beide Zufallsgrößen haben den selben Erwartungswert von \(-\frac{1}{37}\), aber unterschiedliche Varianz}.
\end{center}
\end{figure}

\begin{theorem}[Formel]
	\[ \Var(\alpha X) = \alpha^2 \Var(X) \qquad \forall \alpha \in \R\]
\end{theorem}

\begin{proof}
	...
\end{proof}

\underline{Bemerkung}: Die Varianz lebt also auf einer anderen \enquote{Dimension} oder \enquote{Skala}. Anders gesagt, bei Verdoppeln des Einsatzes vervierfacht sich die Varianz.

\begin{definition}
	\(\sigma(X) \defeq \sqrt{\Var(X)}\) nennt man \textbf{Standardabweichung}.
\end{definition}

\underline{Bemerkung}: Formeln wie \( \Var(X+Y) = \Var(X) + \Var(Y) \) oder \(\sigma(X+Y) = \sigma(X) + \sigma(Y) \) gelten in der Regel \textbf{nicht}.
Beispiel: \( X = Y, \ \ \Var(X+Y) = \Var(2X) = 4 \Var(X) \neq \Var(X) + \Var(X) \), oder \enquote{Gegenwette}: \( X \) = Gewinn des Spielers, \(Y\) = Gewinn der Bank \( \Rightarrow \sigma(X+Y) = 0\).

\begin{theorem}[Formel für Verschiebungen]
	Sei \(X\) eine Zufallsgröße und \(a \in \R \). Dann gilt: \( \E(X+a) = \E(X) + a\) und \( \Var(X+a) = \Var(X)\).
\end{theorem}

\begin{proof}
	\(\Var(X+a) = \E\bigg((X+a - \E(X-a))^2 \bigg) = \E\bigg((X-\E(X))^2\bigg) = \Var(X)\)
\end{proof}

\begin{theorem}[Formel]
\[ \Var(X) = \E(X^2) - \E(X)^2\]
\end{theorem}

\begin{proof}
	\(\Var(X) = \E\bigg((X-\E(X))^2\bigg) = \E\left(X^2 - 2X\E(X) + \E(X)^2\right) \overset{\E \text{ lin.}}{=} \E(X^2) - 2 \E(X) \E(X) + \E(X)^2\)
\end{proof}

\subsection{Ungleichung von Markov und Tschebyschew}
Mit Hilfe von Erwartungswerten von Funktionen von \(X\) kann man Aussagen über \(\P\) treffen. Ist die Varianz z.B. klein, müssen große Abweichungen vom Erwartungswert \textbf{unwahrscheinlich} sein. Dies lässt sich in den Ungleichungen von Markov und Tschebyschew quantifizieren.

\begin{theorem}[Markovungleichung] Sei \( X \) eine Zufallsgröße, \(a \in \R^+, f : \R^+ \to \R^+ \) monoton wachsend (nicht streng!), dann gilt:
\[ \P(\{w : |X(\omega)| \geq a \}) \leq \frac{\E(f(|X|))}{f(a)} \]
\end{theorem}

\underline{Schreibweise}: Man kürzt die Menge \(\{\omega : | X(\omega) | \geq a \} \) auch mit \(X \geq a\) ab. Die Formel wird dann zu:
\[\P(|X| \geq a) \leq \frac{\E(f(|X|))}{f(a)}\]

\begin{theorem}[Sonderfall: Tschebyschew] Markov angewandt auf \( X = Y - \E(Y), f(X) = X^2 \)

\[ \P\bigg(\big|Y-\E(Y)\big| \geq a \bigg) \leq \frac{\E\left(\big(Y - \E(Y)\big)^2\right)}{a^2} = \frac{\Var(Y)}{a^2}\]	
\end{theorem}

\begin{proof}
Wähle zunächst \( f = \operatorname{id} \).

z.z.: \( \P(|X| > a) \leq \frac{\E(|X|)}{a} \).

\(\E(|X|) = \sum\limits_{b\in A} b \cdot \P(|X| = b) \), mit \( A \) ist Bildmenge von \(\Omega\) unter \(|X|\)

\[ \E(|X|) = \underbrace{\sum\limits_{\substack{b\in A \\ b < a}} b \cdot \P(|X| = b)}_{\geq 0} + \sum\limits_{\substack{b\in A \\ b \geq a}} b \cdot \P(|X| = b) \overset{b \geq 0 }{\geq } \sum\limits_{\substack{b\in A \\ b > a}} b \cdot \P(|X| = b) \geq \sum\limits_{\substack{b\in A \\ b > a}} a \cdot \P(|X| = b) = \] \[ = a \cdot \sum\limits_{\substack{b\in A \\ b > a}} \P(|X| = b) = a \cdot \P(|X| > a ) \]

\underline{Allgemeiner Fall:} Wähle \( X = f(|Y|) = | f(|Y|)| \) in \enquote{z.z.}  \( \P(|f(|Y|)| \geq a) \leq \frac{\E(|f(|Y|)|)}{a}\).

Dies gilt für alle \( a \). Wähle \( a = f(b) \) für \( b \in \R^+ \). \( \P(| f(|Y|)| \geq f(b)) \overset{\text{\tiny{monoton}}}{=} \P(|Y| \geq b) \leq \frac{\E(f(|Y|))}{f(b)} \)
\end{proof}

\begin{bsp}
Sei \(\Omega = \left\{\epsdice{1}, \epsdice{2}, \epsdice{3}, \epsdice{4}, \epsdice{5}, \epsdice{6} \right\}\), \( X : \) Augenzahl (z.B. \(X\left(\epsdice4\right) = 4\)), \( Y = \pm 1 \) falls die Augenzahl gerade/ungerade ist.

\[ \begin{array}{ll}\E(X) = 1 \cdot \frac16 + 2 \cdot \frac16 + \ldots + 6 \cdot \frac16 = 3,5 & \E(Y) = 1 \cdot \frac12 - 1 \cdot \frac12 = 0 \\
\Var(X) = \frac16(2,5^2 + 1,5^2 + 0,5^2 + 0,5^2 + 1,5^2 + 2,5^2) = \ldots & \Var(Y) = \frac12 \cdot 1^2 + \frac12 \cdot (-1)^2 = 1 \end{array}\]

\( \P(|Y-0|^2 \geq 1) \leq \frac11 \)

\(\P(|Y-0|^2 \geq 2 ) \leq \frac14 \) (Wahrscheinlichkeit dafür, dass die Abweichung von \(\E(Y)\) größer / gleich 2 ist. Diese ist in Wirklichkeit 0)
\end{bsp}

\subsection{Mehrere Zufallsgrößen}

Wir möchten Unabhängigkeiten bzw. Korrelationen von zwei oder mehr Zufallsgrößen untersuchen.

\begin{definition}
	Seien \(X,Y : \Omega \to \R\) Zufallsgrößen, \(A \defeq X(\Omega), B \defeq Y(\Omega) \). \\
	\(X\) und \(Y\) heißen \textbf{unabhängig}, falls für jedes \(a \in A\) und jedes \(b \in B\) die Ereignisse \(X = a\) und \(Y = b\) unabhängig sind.
\end{definition}

\begin{definition}
	Seien \(X,Y : \Omega \to \R\) Zufallsgrößen, dann bezeichnet man
	\[ \Cov(X,Y) \defeq \E(X\cdot Y) - \E(X) \cdot \E(Y) \]
	als \textbf{Kovarianz} von \(X\) und \(Y\).
\end{definition}

\underline{Bemerkung}: \(\Cov(X,X) = \Var(X)\), \(\Cov(X+b,Y) = \Cov(X,Y)\)

\begin{theorem}
	Falls \(X\) und \(Y\) unabhängig sind, dann ist \(\Cov(X,Y) = 0\)
\end{theorem}

\begin{definition}
	Falls \(\Cov(X,Y) = 0\) nennt man \(X\) und \(Y\) \textbf{unkorreliert}.
\end{definition}

\begin{proof}
	....
\end{proof}

\begin{theorem}[Formel]
	Seien \(X, Y\) Zufallsgrößen, dann gilt
	\[ \Var(X+Y) = \Var(X) + \Var(Y) + 2 \Cov(X,Y) \]
	Insbesondere gilt für unkorrelierte \(X,Y\) (z.B. unabgängig):
	\[ \Var(X+Y) = \Var(X) + \Var(Y)\]
\end{theorem}

\begin{proof}
	...
\end{proof}

Die Kovarianz ist also ein Maß für die Korrelation. Jedoch gilt \(\Cov(2X, 2Y) = 4 \Cov(X,Y)\), d.h. die Kovarianz ist Dimensionsabhängig. Daher definiert man einen Korrelationskoeffizienten folgendermaßen:

\begin{definition}
	Seien \(X,Y\) Zufallsgrößen mit \(\Var(X), \Var(Y) \neq 0\). Dann nennt man
	\[ \kappa \defeq \frac{\Cov(X,Y)}{\sqrt{\Var(X)\cdot\Var(Y)}}\]
	den \textbf{Korrelationskoeffizienten}.
\end{definition}

\begin{theorem}
	Es gilt für alle \(X,Y\) mit \(\Var(X), \Var(Y) \neq 0\):
	\[ \kappa = \frac{\Cov(X,Y)}{\sqrt{\Var(X)\cdot\Var(Y)}} \in [-1,1]\]
	wobei \(|\kappa(X,Y)| = 1 \Leftrightarrow X = a \cdot Y + b \) mit Wahrscheinlichkeit 1 gilt für \(a,b \in \R \) (also \(\P(\{\omega \mid X(\omega) = a \cdot Y(\omega) + b \}) = 1\)).
\end{theorem}
\begin{proof}
	...
\end{proof}

\subsection{Das schwache Gesetz der großen Zahlen}

Das Gesetz macht eine Aussage über den Vergleich der Modellgrößen (Wahrscheinlichkeiten) mit den empirischen Größen (relative Häufigkeiten). Es besagt, dass sich typischerweise die relative Häufigkeit bei oftmaligen Wiederholungen des Experiments (\enquote{große Zahlen}) der Wahrscheinlichkeit annähert.

\begin{theorem}
	Seien \(X_1, X_2, \dots, X_n \) (paarweise) unabgängige Zufallsgrößen mit \(\E(X_i) = \mu \), \(\Var(X_i) = \sigma^2 \ \forall i \in \{1, \dots, n\}\). Dann gilt:

	\[ \P\left(|\overline{X_n} - \mu| \geq \varepsilon \right) \leq \frac{\sigma^2}{n \varepsilon^2} \quad \forall \varepsilon > 0 \]
	mit \(\overline{X_n} = \frac1n \sum\limits_{i=1}^n X_i\) (empirisches Mittel).
\end{theorem}

\underline{Bemerkung}: \(\overline{X_n}\) ist das empirische Mittel. Falls \(X : \Omega \to \{ 0,1 \}\) ist es die relative Häufigkeit eines Treffers (\(\omega\) mit \(X(\omega) = 1\)). \(\mu\) ist der Erwartungswert, also eine theoretische Größe. Falls \(X: \Omega \to \{0,1\}\) ist \(\mu\) die Wahrscheinlichkeit für einen Treffer.

\begin{proof}
	...
\end{proof}

\begin{bsp}
	Wir werfen eine faire Münze \(n = 10000\) mal. Wie groß ist die Wahrscheinlichkeit, dass die relative Häufigkeit von \enquote{Kopf} um mehr als \(10\%\) von \(\dfrac12\) abweicht?

	\(\mu = \frac12 \) (Erwartungswert), \(\sigma^2 = \frac14\).

	\( X: \text{Kopf} \mapsto 1, \text{Zahl} \mapsto 0, \qquad \E(X) = \frac12 = 1 \cdot \frac12 + 0 \cdot \frac12, \qquad \Var(X) = \sigma^2 = \E(X^2) - \E(X)^2 = 1 \cdot \frac12 + 0 \cdot \frac12 - (\frac12)^2 = \frac14 \)

	\[ \Rightarrow \P(|\overline{X_n} - \frac12 | \geq 0,1) \leq \dfrac{\frac14}{10000 \cdot \frac1{100}} = \frac{1}{400} = 0,25\% \] 
\end{bsp}

\underline{Bemerkung}: Es sei \((X_n)_{n\in \N}\) eine Folge (paarweise) unabhängiger Zufallsgrößen mit jeweils \(\E = \mu, \Var = \sigma^2\), dann gilt \( \forall \varepsilon > 0 \)
\begin{itemize}
	\item \( \lim\limits_{n\to\infty} \P(|\overline{X_n} - \mu| \geq \varepsilon ) = 0 \)
	\item \( \lim\limits_{n\to\infty} \P(|\overline{X_n} - \mu| < \varepsilon ) = 1 \)
\end{itemize}

Falls man über Folgen mit unabhängigen Zufallsgrößen sprechen möchte, muss man zunächst die vorherigen Kapitel auf überabzählbare \(\Omega\) verallgemeinern, z. B. \(\Omega \) = Menge von 0-1 Folgen (Ja/Nein Experiment), \( \Omega = [0, 1] \) etc. Naiv könnte man so vorgehen:

\( |\Omega | = \) überabzählbar.
\(\P : \mathcal{P}(\Omega) \to \R \), z.B. \( \P: \mathcal{P}([0,1]) \to \R \)

Die möglichen Maße \(\P\) sind jedoch sehr eingeschränkt!. Insbesondere findet man kein translationsinvariantes Maß auf \([0,1[\).

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=4cm]{Bilder/verschiebung.jpg}
		\caption{Unter einer Verschiebung soll sich das Maß nicht verändern.}
		\label{BildVerschiebung}
	\end{center}
\end{figure}

Die Lösung des Problems ist es, \(\P\) nicht auf ganz \(\mathcal{P}(\Omega)\) zu definieren. Wir entfernen problematische Teilmengen von \(\mathcal{P}(\Omega)\), achten aber darauf, dass der Definitionsbereich von \(\P\) das Sprechen über Gegenereignis oder Vereinigungen etc. noch zulässt.

\subsection{Einschub: Nicht-Lokalität, Korrelationen in der Natur}

\underline{Gedankenexperiment}: Beim Korrigieren einer Klausur oder mündlichen Prüfung stellen wir Korrelationen in den Resultaten fest. Annahme: Falls Student A richtig/falsch liegt, ist auch Student B richtig/falsch. \( \Rightarrow \) Es wurde abgeschrieben (Erklärung für Korrelation. Mögliche andere Erklärung: Fehler im gemeinsamen Skript.)

Modifikation des Experiments: Wir prüfen A und B gleichzeitig in einer mündlichen Prüfung. Die beiden Prüfer P, Q haben je zwei Aufgabensätze S und T zur Verfügung. Die Prüfer können bei jeder Frage zwischen S und T wechseln.

Frage: Gibt es eine Möglichkeit anhand der Daten zu beweisen, dass A und B \textbf{während} der Prüfung kommuniziert haben?

Antwort: Ja!

\begin{bsp}
	\begin{enumerate}[a)]
		\item P wählt zufällig S oder T bei jeder Frage. Am Abend treffen sich P und Q und stellen fest: B (von Q geprüft) sagt jedes mal \enquote{ja}, wenn P sich für S entschieden hatte, \enquote{nein} sonst.

		\begin{center}	
			\begin{tabular}{cccccccl}
			 Frage & 1 & 2 & 3 & 4 & 5 & 6 & \(\dots\) \\
			 P & S & S & T & S & T & T & \(\dots\) \\
			 A & j & n & n & j & j & n & \(\dots\) \\\hline
			 Q & T & S & S & T & T & S & \(\dots\) \\
			 B & j & j & n & j & n & n & \(\dots \Rightarrow \) kommuniziert.
			\end{tabular}
		\end{center}

		\item Die Antworten sind perfekt korreliert. Falls man beide T oder S fragt, sagen beide das selbe, mal \enquote{ja} mal \enquote{nein}, falls man A T und B S fragt, ebenso. Falls man A S und B T fragt, genau anders (ja-nein oder nein-ja).

		\begin{center}
			\begin{tabular}{cccccccccc|c|}
			 Frage & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\
			 	 P & T & T & S & T & T & S & S & S & T & T | S\\
			 	 A & j & j & j & n & n & j & n & j & j & j | j \\\hline
			 	 Q & T & S & S & S & S & T & S & T & T & T | S\\
			 	 B & j & j & j & n & n & n & n & n & j & j | j.
			\end{tabular}	
		\end{center}

		Eine \enquote{Absprache} vor der Prüfung, die alle 4 Regeln erfüllt, gibt es nicht. Auch hier wurde kommuniziert.	
	\end{enumerate}
\end{bsp}

Der Unterschied zwischen Beispiel 1 und Beispiel 2 ist, dass im ersten aus den Antworten von B auf die Fragen von P geschlossen werden kann: Wenn B \enquote{ja} antwortet, hat P eine Frage aus S gestellt, ansonsten aus T. Im zweiten Beispiel lassen sich keine Schlüsse aus den Antworten von B auf Fragen von P oder Antworten von A ziehen. Insofern wird bei Beispiel 1 mehr Informationen \enquote{übertragen}.

In der Natur tritt ein ähnliches Phänomen wie in Beispiel 2 auf: Photonen können in einem Zustand erzeugt werden, den man als Quantenverschränkung bezeichnet, wonach Messungen an dem Zustand eines Photons mit den Messungen des Zustands am anderen Photon korrelieren. Die Korrelation lässt sich nur mit einem nicht-lokalen Zustand der Photonen erklären. Es werden jedoch keine Informationen über die Messung am jeweils anderen Photon übertragen.