\section{Hypothesentests}
Das Finden des richtigen Modells ist ein ganz entscheidender Punkt in der angewandten Stochastik. Wir werden verschiedene Modelle an Hand ihrer \enquote{Plausibilität} überprüfen und vergleichen. \\
Bisher haben wir ausgehend vom Modell die Wahrscheinlichkeit für gewisse Ausgänge von Experimenten bestimmt. Die Eigentliche Frage geht meist genau in die andere Richtung.

\subsection{Maximum Likelyhood}
Gegeben ein Wahrscheinlichkeitsmodell mit einem (oder mehreren) Parametern. Frage: Welcher Wert dieser Parameter ist an Hand eines experimentellen Befundes der plausibelste.

\begin{bsp}Blitzschläge in Bayern. Wie ist die Anzahl an Blitztreffen pro Jahr? Klar: Poissonverteilung.
\[f(k) = \frac{\lambda^2}{k!}e^{-\lambda}\]
\(\lambda\) ist der unbekannte Parameter. \\
Annahme: In 2016 gab es 5 Häuser, die in Bayern vom Blitz zerstört wurden. Idee: Wähle das \(\lambda\), für welches diese gegebene Anzahl maximale Wahrscheinlichkeit hat.
\[p(\lambda) = \P(5 \text{ Treffer, falls } \lambda \text{ geg.}) = \frac{\lambda^5}{5!}e^{-\lambda}\]

\begin{figure}[h]
 \centering
 \includegraphics[width=7cm]{Bilder/b14.jpg}
 %\caption{}
 \label{Bild14}
\end{figure}

\[p'(\lambda) = \frac{5 \lambda^4}{5!}e^{-\lambda}+\frac{\lambda^5}{5!}e^{-\lambda}(-1) = \frac{\lambda^4}{5!}e^{-\lambda}(5-\lambda) = 0 \; \text{falls}\; \lambda = 5\]
\(\Rightarrow(\lambda)\) hat sein Maximum an der Stelle \(\lambda = 5\). \\
Für \(\lambda = 5\) ist das experimentelle Ergebnis am plausibelsten. \(p(\lambda = 5) = \frac{5^5}{5!}e^{-5} = \dots\).

Das gilt auch für mehr als ein experimentelles Resultat:

\begin{center}\begin{tabular}{c|c}
Jahr & Treffer \\ \hline
2016 & 5 \\
2015 & 1 \\
2014 & 2 \\ 
\end{tabular}
\end{center}
\[p(\lambda) = \frac{\lambda^5}{5!}e^{-\lambda} \cdot \frac{\lambda^1}{1!}e^{-\lambda} \cdot \frac{\lambda^2}{2!}e^{-\lambda} = C \cdot \lambda^8 e^{-3\lambda}\]
\[p'(\lambda) = C \lambda^7 e^{-3 \lambda}(8-3\lambda)\]
\(\Rightarrow \lambda = \frac{8}{3}\) ist Wahrscheinlichkeit, wo \(p(\lambda)\) maximal wird.
\end{bsp}

\begin{theorem}
	Die Methodik funktioniert auch für mehr als einen Parameter (z.B. \(\lambda, \mu\)). Man maximiert endsprechend \(p(\lambda,\mu)\). Hier sollte man mehr als einen experimentellen Befund haben.
\end{theorem}

\subsection{Hypothesentest - Framework}

\underline{Beispiel:} 500 maliger Münzwurf. Stichprobe:
\begin{center}\begin{tabular}{c|c}
Unterscheidungseinheit i & Merkmal \(x_i\) \\ \hline
1&1\\
2&0\\
3&0\\
\dots&\dots\\
500&1\\ 
\end{tabular}\end{center}
\[X_i : ( \Omega_1, F_1, \P) \rightarrow (\Omega_2, F_2, \P_{x_i})\]
\[\Omega_1 = \{\text{Kopf}, \text{Zahl}\} \quad \Omega_2=\{1,0\}\]
\[X^{(n)} = \sum_{i=1}^n X_i\]
\[X_i : ( \Omega_3, F_3, \P^{(n)}) \rightarrow (\Omega_4, F_4, \P_{x^n})\]
\(\Rightarrow \)Statistisches Modell \(\varepsilon = (\Omega_4, F_4,P_{x^n})\)
Sei \(X_i \sim B(1,p) \)
\[ \Rightarrow X^{(n)} \sim B(n,p)\]
\[P_{X^n} = \{P_{X^n} \sim B(n,p), p\in [0,1]\}\]
Zum Beispiel: \(H_0 : p = 0,5 \) (entspricht \(B(n,0,5)\)) \\
\(\Rightarrow\) Annahmen:
\begin{itemize}
	\item \(X_i\) unabh und identisch verteilt (Modellannahme)
	\item \(X_i\) Binomialverteilt (Modellannahme)
	\item \(p=0,5\) (Annahme der Hypothese (wird getestet)) \\ Zum Beispiel gegen \(H_1: p=0,55\)
\end{itemize}

\underline{Hypothesentest}
Entscheidungsregel über (Un-)gültigkeit eines Hypothesentests

\begin{tabular}{c|c|c}
mögliche Ausgänge & \(H_0\) wahr & \(H_0\) falsch \\ \hline
\multirow{2}{*}{\(H_0\) wird nicht abgelehnt} & \multirow{2}{*}{\huge{\checkmark}} & Fehler 2. Art \\ 
&& \(\beta = P( H_0\text{ nicht abgelehnt }| H_0 \text{ falsch })\)\\\hline
\multirow{2}{*}{\(H_0\) wird abgelehnt} & Fehler 1. Art & \multirow{2}{*}{\huge{\checkmark}} \\
& \(\alpha = P( H_0 \text{ abgelehnt }| H_0 \text{ wahr})\) & \\
\end{tabular}

\underline{Signifikanzniveau:} (eines Fehleres) \\
vorgegebene Maximalwert für \(\alpha\) (z.B: \(\alpha = 0,5\)) \\

\underline{Vorgehen:}
\begin{enumerate}
	\item Bestimme Prüfgröße aus Stichprobe zu der die Verteilung unter \(H_0\) bekannt ist
	\item Bestimme Annahme- und Ablehnungsbereich, sodass (unter \(H_0\)) die Wahrscheinlichkeit für den Fehler 1. Art \(\alpha\) nicht überschreitet
\end{enumerate}

\underline{Bemerkung:}
\begin{itemize}
	\item Wird \(H_0\) abgelehnt, muss \(H_1\) nicht zwingend wahr sein. (Andere Hypothesen könnten die Daten auch plausibel erklären).
	\item Wird \(H_0\) nicht abgelehnt, muss \(H_0\) nicht zwingend wahr sein. (Es könnte auch andere Modellannahme verletzt sein). 
\end{itemize}
\begin{theorem}
	Die Korrektheit von Hypothesen kann niemals (sicher) bestätigt werden!!
\end{theorem}

\subsection{Alternativhypothese \(H_1\)}
\begin{enumerate}
	\item \underline{einseitig:} \(H_1\) nur auf einer Seite von \(H_0\) \\ z.B. \(H_0: p=0,5 \text{ vs. } H_1: p= 0,55\) 
	\item \underline{zweiseitig:} \(H_1\) nur auf beiden Seiten von \(H_0\) \\ z.B. \(H_0: p=0,5 \text{ vs. } H_1: p \neq 0,5\)
\end{enumerate}

\subsection{p-Wert (eines Tests)}
Wahrscheinlichkeit unter \(H_0\) für eine wie in der Stichprobe beobachtete oder in Richtung \(H_1\) extremere Prüfgröße.

\begin{bsp}[einseitiger Test]
\(H_0: p= 0,5 \text{ vs. } H_1: p = 0,55 \;\;\; \alpha=5\%\)
\[X_i \overset{iid}{\sim} B(1,p) \; \; \mu = p, \sigma^2 = p(1-p)\]
Wegen dem zentralen Grenzwertsatz:
\[\Rightarrow T:=\frac{1}{\sigma \sqrt{n}}(\sum_{i=1}^{n} X_i - n\mu) \overset{q}{\sim} N(0,1)\]
unter \(H_0\) mit \(n=500 \; \mu=0,5 \; \sigma = \sqrt{0,25}\)
\[T_{H_0}^{(500)}= \frac{1}{\sqrt{0,25 \cdot 500}}(X^{(500)}-250) \overset{q}{\sim} N(0,1)\]

\begin{figure}[h]
 \centering
 \includegraphics[width=7cm]{Bilder/b15.jpg}
 %\caption{}
 \label{Bild15}
\end{figure}

\underline{Entscheidungsregel:} Lehne \( H_0 \) ab, wenn \( t^{500} \geq t_\text{krit.} = 1,64 \), sonst lehne \( H_0 \) nicht ab.

Sei nun \( x^{(500)} = 263 \) mal \enquote{Kopf} geworfen wurde.

\( t_{H_0}^{(500)} = \frac{1}{\sqrt{125}}(x^{(500)} - 250) = \frac{13}{\sqrt{125}} \approx 1,16 < 1,64 = t_\text{krit.}\). Somit wird \( H_0 \) nicht abgelehnt.

\end{bsp}

\begin{bsp}
\(H_0\): \enquote{Münze ist fair}, also \( p = \frac12 \)

Signifikanzniveau wird vorgegeben (Fehler 1. Art): \(\alpha = 5 \%\). Stichprobengröße \(n = 10000\). Annahme \(H_0 \) stimmt.

\begin{figure}[h]
 \centering
 \includegraphics[width=7cm]{Bilder/b16.jpg}
 %\caption{}
 \label{Bild16}
\end{figure}
\end{bsp}

\textbf{\textit{Wie hängt das Signifikanzniveau mit dem Wurzel-n-Gesetz zusammen?}}
\begin{center}
	\begin{minipage}{15.5cm}
\(p = \frac12, \Var: \frac12 \cdot \frac12 \Rightarrow \sigma = \frac12\)

Bei \(n\) unabhängigen Wiederholungen: \(\sigma = \frac{\sqrt{n}}{2} \Rightarrow \sqrt{n} \widehat{=} 2 \sigma \)

\underline{Praktische Vorgehensweise}: \(\alpha\) gegeben, 
rechne \( \alpha \) mit Tabelle in \(\sigma\) um: 

\[ \begin{array}{rcl} \alpha = 5 \% &\widehat{=} &2\ \sigma  \\
\text{bzw. allgemein:} \qquad \alpha = x \hphantom{\%} & \widehat{=} & y\ \sigma \end{array}\]

mit Ablehnungsbereich \([ n \mu - y \sigma, n \mu + y \sigma ]\)
\end{minipage}
\end{center}

Der p-Wert gibt an, wie hoch die Wahrscheinlichkeit einer Abweichung größer oder gleich der Abweichung des Experimentes ist. (Vorausgesetzt \(H_0\) trifft zu.)

Z.B. \(n \mu = 5000, S_n = 5123 \Rightarrow \) Abweichung: \( |S_n - n \mu | = 123\). p-Wert \(= \P_{H_0}(|S_n - n\mu| \geq 123) \)

\underline{Bemerkung}: Die obigen Rechnungen setzen die Gültigkeit der Nullhypothese voraus. Diese stellen wir aber in Frage. Da gibt es leider keinen Ausweg. Um rechnen zu können brauchen wir ein Modell. Was wir also bestimmen ist folgendes:

Gegeben \(H_0\) stimmt, wie hoch ist die Wahrscheinlichkeit, dass mein Experiment einen gewissen Ausgang hat (z.B. \(S_n\) liegt im Ablehnungsbereich).

Aber \(S_n\) steht außer Frage, aber \(H_0\) gilt es zu überprüfen. Eigentlich hätten wir gerne die Wahrscheinlichkeit bestimmt, dass unter der Voraussetzung z.B. \(S_n = 5123\) \(H_0\) zutrifft bzw. nicht zutrifft.

\begin{center}\begin{tabular}{c|c|c}
 & \(H_0\) trifft zu & \(H_0\) trifft nicht zu \\ \hline

\multirow{2}{*}{\(S_n \in I\)} & \multirow{2}{*}{\(1 - \alpha\)} & \textit{\small{hier kann man zwar was reinschreiben,}} \\
& & \textit{\small{es aber nicht ausrechnen}} \\ \hline
 \(S_n \notin I\) & \multirow{2}{*}{\(\alpha\)} & \textit{\small{hier kann man zwar was reinschreiben}}  \\
Ablehung & & \textit{\small{es aber nicht ausrechnen}} \\ 
\end{tabular}\end{center}


\subsection{Einseitiger Test}

\underline{Motivation}: Wir haben den Verdacht, dass eine Münze für ein Spiel gezinkt wurde, so dass zu unseren Ungunsten, \(p > \frac{1}{2}\) (\(\overline{H_0}\)) ist. 
\begin{figure}[h]
 \centering
 \includegraphics[width=7cm]{Bilder/b17.jpg}
 \caption{\( n = 10000, \alpha = 5 \% \)}
 \label{fig:17}
\end{figure}


\(H_0: p = \frac12\). Falls wir eine Abweichung erhalten, die im Ablehnungsbereich (im rechten Teil) liegt, schöpfen wir Verdacht. Praktisch: \(n (=10000) \) gegeben, \(\alpha (=5\%) \) gegeben. Dieses \(\alpha\) entspricht \(2\alpha\) beim zweiseitigen Test (Siehe Skizze \ref{fig:17}). Die Tabelle gibt uns Abweichung in \(\sigma's\): Bei uns \(1,64 \sigma\), Ablehnungsbereich von \(H_0\): \([n \mu + 1,64 \cdot \sigma_n, \infty[ = [5082, \infty[\)

Es könnte sein, dass die Alternative zu \(p = \frac12\) klar formuliert ist, zum Beispiel: \[p = \frac23 \qquad H_0 : p = \frac12 \qquad H_1 : p = \frac23 \]
Man gibt sich meist einen \(\alpha\)- Wert (= Fehler 1. Art) für eine der Hypothesen (in der Regel \(H_0\)) vor. Wie oben bestimmt man den Ablehnungsbereich.

Dadurch, dass \(H_1\) klar formuliert ist, können wir auch die Wahrscheinlichkeit bestimmen, dass \(H_0\) \textbf{nicht} abgelehnt wird, obwohl \(H_1\) stimmt. Diese Wahrscheinlichkeit nennt man Fehler 2. Art (\(\beta\)).

\begin{center}
\begin{tabular}{c|c|c}
& \(H_0\) trifft zu & \(H_1\) trifft zu \\ \hline
\( S_n \in I_1 \) & \(1- \alpha\) & \(\beta\) \\ \hline
\(S_n \in I_2\) & \(\alpha\) & \(1- \beta\)
\end{tabular}
 \end{center} 
\(I_1 \cup I_2 = \R\), \(I_2\) ist Ablehnungsbereich von \(H_0\). 

 \(\alpha\) ist gegeben, \(\beta\) wird berechnet. \(\left(\alpha \xrightarrow{\text{berechne}} I_1 \text{ und } I_2 \xrightarrow{\text{berechne}} \beta \right)\)

 Es wäre gut, wenn beide Fehler klein sind. Jedoch gibt man \(\beta\) nicht vor, sondern berechnet es. Achtung: Bei Verkleinerung von \(\alpha\) vergrößert sich \(\beta\).

 \begin{figure}[h]
  \centering
  \includegraphics[width=7cm]{Bilder/b18.jpg}
  %\caption{}
  \label{Bild18}
 \end{figure}

Wenn beide Fehler verkleinert werden sollen, muss man \(n\) entsprechend vergrößern.

\begin{figure}[h]
 \centering
 \includegraphics[width=\textwidth]{Bilder/b19.jpg}
 %\caption{\(\Var = \frac14 \) \\ \(\Var = \frac14 \cdot 10000 \Rightarrow \sigma_n = 50 \)}
 \label{Bild19}
\end{figure}
\todo{Bild unterschrift?}

\subsection{Das Neyman-Pearson Lemma}

Gegeben seien zwei Hypothesen, \(H_0\) und \(H_1\) für ein Experiment.
Auch der Fehler 1. Art \((\alpha)\) soll vorgegeben sein.
Welcher Ablehnungsbereich der Hypothesen \(H_0\) optimiert (minimiert) den Fehler zweiter Art? \\
\underline{Die Antwort gibt das Neyman-Pearson Lemma.}

\begin{figure}[h]
 \centering
 \includegraphics[width=\textwidth]{Bilder/b19.jpg}
 %\caption{}
 \label{bild19}
\end{figure}
(Bilder werden jetzt gleich durch den Text erklärt)

Das Neyman-Pearson-Lemma besagt folgendes: Seien \(\rho_0\) und \(\rho_1\) die Dichten der beiden Hypothesen. Betrachte die Funktion
\[n(x) = \begin{cases}
1 &\text{, falls } \rho_1(x)=\rho_0(x)=0) \\
\infty &\text{, falls } \rho_1(x)=0 \\ 
\frac{\rho_0(x)}{\rho_1(x)} & \text{, sonst} \end{cases}\]

Für jedes \(\gamma \in \R^{+}\) definiere folgende Menge: (Bei D denke man an \(\R\)) 
\[A_\gamma = \{x \in D: n(x) \leq \gamma\}\]
Wähle \(\gamma\) so, dass \(\P_{H_0}(X\in A_\gamma) = \alpha \quad \quad (= \int_{A_\gamma} \rho_0(x)dx)\) \\
Aussage vom Lemma: \(A_{\gamma_0}\) ist für dieses \(\gamma_0\) die Menge mit optimalen \(\beta\). \\
\underline{Begründung:} Wir zeigen, dass eine Abänderung \(\beta\) verschlechtert. \\
\[B = A_{\gamma_0} \\ W \cup D \quad \quad W \subseteq A_{\gamma_0}, D \subseteq A_{\gamma_0}^C \]
Damit immernoch \(\int_B \rho_0(x)dx = \alpha\) ist, muss gelten: \(\int_W \rho_0(x)dx = \int_D\rho_0(x)dx\). \\
Der \(\beta\)-Fehler für \(A_{\gamma_0}\): \(\beta = \int_{A_{\gamma_0}^C} \rho_1(x)dx\).
Für den anderen Ablehnungsbereich B: 
\[\beta^B = \int_{B^C}\rho_1(x)dx = \int_{A_{\gamma_0}^C}\rho_1(x)dx + \int_W \rho_1(x)dx - \int_D \rho_1(x)dx\]

z.z.: \(\int_W \rho_1(x)dx > \int_D \rho_1(x)dx\) \\
Da \(W \subseteq A_{\gamma_0}\) ist \(\frac{\rho_0(x)}{\rho_1(x)} \leq \gamma \forall x \in W\).
\[\Rightarrow \rho_1(x) \geq \gamma^{-1} \rho_0(x) \Rightarrow \int_W \rho_1(x)dx \geq \gamma^{-1} \int_W \rho_0(x) := *\]
Da \(D \subseteq A_{\gamma_0}^C\) \[ \Rightarrow \frac{\rho_0(x)}{\rho_1(x)}>\gamma \Rightarrow \int_D \rho_1(x) < \gamma^{-1} \int_D \rho_0(x)dx = *\]
\(\Rightarrow\) Behauptung!
